const http = require('node:http')

const server = http.createServer((req, res) => {
    // Send response
    res.end('Hello World from the server')
})

server.listen(5000, 'localhost', () => {
    console.log('Server is listening at localhost on port 5000')
})

// from:
// https://dev.to/ericchapman/create-a-backend-in-javascript-part-4-create-your-first-http-server-5k1
